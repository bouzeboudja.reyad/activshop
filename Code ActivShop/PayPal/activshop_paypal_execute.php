<?php

include 'routines.php';
include 'motoclic_conf.php';
require 'activshop_paypal_token.php';

$total_pos_ttc=$_REQUEST['total_pos_ttc'];
//echo " payement : $total_pos_ttc";
//exit;
$order_id=$_GET['order_id'];
$doc_id=$_REQUEST['doc_id'];
$session_id=$_REQUEST['session_id'];
//echo "doc id : $doc_id et order id : $order_id et $total_pos_ttc";
////////////////////////////////////////////////////////////////////////////////////
// INTERROGATION PAYPAL POUR CONNAITRE AVANCEMENT PAIEMENT /////////////////////////
////////////////////////////////////////////////////////////////////////////////////

$server='www.dupontsarl.store'; // ex : www.duponsarl.store

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$paypal_url/v2/checkout/orders/$order_id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    'authorization: Bearer '.$token,
    'content-type: application/json'
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err)
{
  echo "cURL Error #:" . $err;
}
else
{
	$array=json_decode($response, true);
	//////////////DEBUG AFFICHAGE REPONSE PAYPAL //////////
	//print("<pre>".print_r($array,true)."</pre>");
	///////////////////////////////////////////////////////

	if ($array['status']=="COMPLETED") // Si le paiement a été effectué et reçu
	{
			// Succès : la commande a été effectuée et executée avec succès.

//echo "xx $doc_id xx $order_id";			// Penser à mettre en HTTPS pour la prod
		header("Location: http://".$server."/activshop-paypal-success?return=success&session_id=$session_id&total_pos_ttc=$total_pos_ttc&doc_id=$doc_id&order_id=".$order_id);
//echo "reussite";


	}
	else
	{
			// Erreur : la commande n'est pas réglée.

//echo "xx $doc_id xx";			// Penser à mettre en HTTPS pour la prod
		header("Location: http://".$server."/activshop-paypal-success?return=notpaid&session_id=$session_id&doc_id=$doc_id&order_id=".$order_id);
//echo "echec";
	}

}
?>
