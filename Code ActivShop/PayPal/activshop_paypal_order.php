<?php
 // order.php
// Boutique en ligne : Création d'une commande
/* Version 2021-03-18 */

/*
Template Name: activshop_paypal_order
*/

get_header(); // On affiche l'en-tête du thème WordPress

include_once 'routines.php';
include "motoclic_conf.php";

$server=$_SERVER['SERVER_NAME'];  // ex : www.duponsarl.store

//echo "xx $server xx";



// echo "test"; exit;
require "activshop_paypal_token.php";

//echo "ww $paypaluser ttt $paypalpass ww";
$total=$_REQUEST['total'];
$delivery_mode_cocher_price=$_REQUEST['delivery_mode_cocher_price'];
$total_pos_ttc=$_REQUEST['total_pos_ttc'];
//echo "ouiiiii $total_pos_ttc et  $delivery_mode_cocher_price ouiiiii";
//exit;
$session_id=$_REQUEST['session_id'];
$taux_tva=$_REQUEST['taux_tva'];
$doc_id=$_REQUEST['doc_id'];
//echo "Document : $doc_id et $session_id<br>";
// Préparation de la Commande
$lang=_request('lang'); choixlang($lang);
$debug=_request('debug');
$today = date ("Y-m-d");

try {
        $dbh = new PDO("mysql:host=sqlserver;dbname=$paypaldatabase", $paypaluser, $paypalpass);
 	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Activation de l'affichage des exceptions (erreurs) MySQL dans le catch
} catch(Exception $e) {
        echo 'Erreur : '.$e->getMessage().''; echo 'N° : '.$e->getCode();
}

/*
// ---------- Calcul numéro de commande --------
$requete="
SELECT reference_commande
FROM activpart.paypal_orders
ORDER BY LENGTH(reference_commande) DESC, reference_commande DESC 
LIMIT 1
";
$result=$dbh->query($requete);
foreach ($result as $row) {
    $reference_commande =$row['reference_commande'];
}
// ------------------------------------------------------------
*/

if (!isset($reference_commande))
{$reference_commande="C-1";}
else {
$reference_commande=explode("-", $reference_commande);
$dernier_numero=$reference_commande[1];
$reference_commande="C-".($dernier_numero + 1);
}

include "../db_connect.php";

// ------- Chargement entête du document -----
$requete="
SELECT doc_number,
third_civil_code, third_nom, third_prenom,
third_adresse1, third_adresse2,
third_codepostal, third_ville, pays_doc,
doc_tel, doc_email,
total_pos_ttc
FROM documents
WHERE doc_id='$doc_id'
";
// echo "xx $requete xx";
$result = $wpdb->get_results($requete);
// var_dump($result);
foreach ($result AS $row ) {
      $doc_number= $row->doc_number; //echo "xx DOC_NUMBER $doc_number xx<br>";
      $client_civil_code= $row->third_civil_code;
      $client_nom= $row->third_nom;
      $client_prenom= $row->third_prenom;
      $client_adresse1= $row->third_adresse1;
      $client_adresse2= $row->third_adresse2;
      $client_codepostal= $row->third_codepostal;
      $client_ville= $row->third_ville;
      $client_pays= $row->pays_doc;
      $client_tel= $row->doc_tel;
      $client_email= $row->doc_email;
      $total_pos_ttc= $row->total_pos_ttc;
}
//echo "non $total_pos_ttc non";
// ---- Fin Chargement de la commande --------
//echo "xxx $doc_id xxx";
// ------- Chargement des lignes du document -----
$requete="
SELECT pos_num,pos_description, pos_puttc, pos_qte, total_ttc_pos,
pos_reference,
CASE WHEN pos_description IN ('','NULL')
THEN pos_reference
ELSE pos_description
END pos_description
FROM doc_positions
WHERE doc_id='$doc_id'
";
//echo "xx $requete xx";
$result = $wpdb->get_results($requete);
 //var_dump($result);
foreach ($result AS $row ) {
      $pos_num= $row->pos_num;
      $pos_reference= $row->pos_reference;
      //$pos_description= "MALOSSI 10 GR";
      $pos_description= $row->pos_description;
      //echo "xxx $pos_description xxx";
      $pos_puttc= $row->pos_puttc;
      $pos_qte= $row->pos_qte;
      $total_ttc_pos= $row->total_ttc_pos;

$items.=' {
  "name": "'.$row->pos_description.'",
  "unit_amount": {
      "currency_code": "EUR",
      "value": "'.$row->pos_puttc.'"
  },
  "quantity": "'.intval($row->pos_qte).'",
  "description": "'.$row->pos_description.'",
  "sku": "'.$row->pos_reference.'" } ,
';
} // Fin de foreach 
$items= substr($items, 0, -2); // On enlève la dernière virgule
// echo "xx $items xx<br"; 



$prix_unitaire_remise=round($prix_vente*(100-$remise_percent)/100,2);
$total_ht=$prix_unitaire_remise*$qte_choisie;
$total_ttc=round($total_ht*(1+($taux_tva/100)),2);
$taxe_totale=$total_ttc-$total_ht;
$taxe_unitaire=$taxe_totale/$qte_choisie;

//Fin de la préparation de la commande

//Création de la commande

$curl = curl_init();

// --- Penser à repasser en HHTP pour la mise en prod ----
$total_with_delievery=$total_pos_ttc+$delivery_mode_cocher_price;
$return_url = "http://".$server."/activshop-capture/?session_id=$session_id&doc_id=$doc_id&total_pos_ttc=$total_pos_ttc&total_with_delievery=$total_with_delievery";

$cancel_url = "http://".$server."/produits";

$postfields= '

{
"intent": "CAPTURE",
"payer": {
       "name" : {
             "given_name": "'.$client_prenom.'",
             "surname": "'.$client_nom.'"
       },
       "email_address" : "'.$client_email.'",
	"phone": {
       	    "phone_type": "MOBILE",
       	    "phone_number": {
       		        "national_number": "'.$client_tel.'"
	    }
       	},
	"address" : {
		  "address_line_1": "'.$client_adresse1.'",
		  "address_line_2": "'.$client_adresse2.'",
		  "admin_area_2": "'.$client_ville.'",
		  "admin_area_1": "CA",
		  "postal_code": "'.$client_codepostal.'",
		  "country_code": "FR",
		  "phone" : "'.$client_tel.'"
	},
	"shipping_address": {
		"recipient_name": "'.$client_nom.'",
          	"line1": "'.$client_adresse1.'",
          	"line2": "'.$client_adresse2.'",
          	"city": "'.$client_ville.'",
                "phone" : "'.$client_tel.'"
	}
},
"purchase_units": [
         {
	"reference_id": "'.$doc_number.'",
        "amount": {
		"currency_code": "EUR",
                "value": "'.$total_pos_ttc.'",
                "breakdown": {
                	"item_total": {
                                 "currency_code": "EUR",
                                 "value": "'.$total_pos_ttc.'"
                        },
                        "tax_total": {
                                 "currency_code": "EUR",
                                 "value": "'.$taxe_totale.'"
                        }
                 }
          },
"payee": {
	"email_address": "'.$payee_email_address.'",
        "merchant_id": "'.$payee_merchant_id.'"
},
        "description": "Commande Motoclic",
        "custom_id": "'.$reference_commande.'",
        "soft_descriptor": "MOTOCLIC",
        "items" : [
                 '. $items . '
        ]
}
],
"application_context": {
	"brand_name": "ACTIVPART",
	"locale": "fr-FR",
	"user_action": "PAY_NOW",
	"return_url": "'.$return_url.'",
	"cancel_url": "'.$cancel_url.'"
}
}
';

// echo "<pre>$postfields</pre>"; 

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$paypal_url/v2/checkout/orders",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $postfields,
  CURLOPT_HTTPHEADER => array(
    'accept: application/json',
    'prefer: return=representation',
    'accept-language: fr_FR',
    'authorization: Bearer '.$token,
    'content-type: application/json'
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);


curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
$array=json_decode($response, true);

//////////////////////////DEBUG/////////////////////////////
//print("<pre>".print_r($array,true)."</pre>");
////////////////////////////////////////////////////////////

//echo "xxx  $response xxx";
/*
$wpdb->update("doc_positions",array(
                "total_with_delivery" => $delivery_mode_cocher_price,
                ));
*/
if($array['status']=="CREATED") {

    $wpdb->insert("paypal_orders",array(
                    "paypal_order_response"=>$response,
                    "date_cre"=>$now,
                    "dossier_nom"=>$soc_name,
                    "order_id"=>$array['id'],
                    "dossier_id"=>$dossier_id,
                    "reference_commande"=>$reference_commande,
                    "created"=>'true',
     ));

echo "Commande créée <br>";
$url=$array['links']['1']['href'];
print "<meta http-equiv=refresh content=0;url=$url />";
}
else
{
    echo "<p class=msg_error>Erreur création commande.</p>";
}

}


?>
