<?php
// Page de récapitulation de la commande
// Version 2021-03-31
/*
Template Name: activshop_paypal_success
*/
get_header(); // On affiche l'en-tête du thème WordPress


include 'activshop_style.css';
include "routines.php";
include "motoclic_conf.php";

$total_pos_ttc=$_REQUEST['total_pos_ttc'];
//echo " payement : $total_pos_ttc";
//exit;
$lang=_request('lang');choixlang($lang);
$debug=_request('debug');
$order_id=_request('order_id');
$return=_request('return');
$type_doc_id=$type_doc_son_id='2'; // Type de document destination : Facture client
$source_prog='boutique';
$doc_id=$_REQUEST['doc_id'];
$session_id=$_REQUEST['session_id'];
$finAchat=$_REQUEST['finAchat'];

//echo "$total_pos_ttc et finAchat : $finAchat et session id : $session_id et order_id : $order_id et doc id : $doc_id et type doc : $type_doc_son_id";

$date_payment= date("d/m/Y");
$today = date ("Y-m-d");
//echo "zzzzzz $date_payment zzzzz";

try {
        $dbh = new PDO("mysql:host=sqlserver;dbname=$paypaldatabase", $paypaluser, $paypalpass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Activation de l'affichage des exceptions (erreurs) MySQL dans le catch
} catch(Exception $e) {
        echo 'Erreur : '.$e->getMessage().''; echo 'N° : '.$e->getCode();
}


$titrepage="Récapitulatif de la commande";
$reussite='0';
$server=$_SERVER['SERVER_NAME'];  // ex : www.duponsarl.store
$url="http://".$server."/activshop-produit-list";


print "<HTML>";
print "<head>";
include_once "../includes/style_load.php";
print "<title> $titrepage   - $soc_nom_com </title>";
print "</head>";

print "<BODY>";

print "<div id=conteneur>";

print "<div id=header>"; include "../header_gene.php"; print "</div>";

print "<div id=frame>";

echo "<h1 class=center>Commande validée !</h1>";

print "<p><fieldset align=center style=width:840px;>";
print "<table class=\"trans\" style=\"width:800px\">";
print "<tr>";
//print "<center><br><a href='/activshop-produit-list' 
if ($return=="success") 
{
	print "<td class=\"msg_ok\" style=\"text-align : center\">";  
	print "<p>La commande a été enregistrée au numéro : $order_id </p>";
	print "<p>Merci pour votre commande. Une confirmation du paiement vous sera envoyée</p>";
	print "<p> Vous allez être redirigé vers la boutique.<p>";
//Transformation du bon de commande qui passe en status soldé en facture ouvert
	include "client_doc_dech.php";
// Ajout d'un moyen de paiement afin de mettre à jour le stock du produit
	$doc_payments_transac='insert';
//	echo " payement : $total_pos_ttc";
	$payment_amount = $total_pos_ttc;
	$mode_regl_id = 1;
	include "doc_payments_upd.php";
// Passage du status ouvert de la facture en confirmé
	$doc_id=$new_doc_id;
	$date_doc= $date_payment= $today;
	$transac = 'update';
	$doc_status_code ='2';
	include 'client_doc_upd.php';
print "<meta http-equiv=refresh content=7;url=$url />";
//print "<center><br><a href='/activshop-produit-list/ class=button>Retour</a></center>";
}
//    echo "xxx $doc_id et $type_doc_son_id et $source_prog et $order_id xxx";

//	echo "<br> réussite = $reussite ";
//header("Location: http://www.motoclic.pro/dossiers/dupontsarl/wp-content/themes/ascent/paypal/client_doc_dech.php/?doc_id=$doc_id&type_doc_son_id=$type_doc_son_id&source_prog=$source_prog&order_id=".$order_id);

//header("Location:http://www.motoclic.pro/programs/client_doc_dech.php/?doc_id=$doc_id&type_doc_son_id=$type_doc_son_id&source_prog=$source_prog&order_id=".$order_id);
//header("Location: http://www.google.fr/");
//header("Location: http://".$server."/activshop-paypal-success?return=success&doc_id=$doc_id&order_id=".$order_id);

//    include "client_doc_dech.php";
else if ($return=='already') 
{
    print "<td class=\"msg_warning\" style=\"text-align : center\">";
    print "<p>Commande déjà exécutée.</p>";
}
else if ($return=='notpaid')
{
    print "<td class=\"msg_error\" style=\"text-align : center\">";
    print "<p>Erreur !</p>";
    print "<p>Commande non payée.</p>";
}
print "</td>";
print "</tr>";
print "</table>";
print "</fieldset>";
print "</div>";

print "</BODY>";

print "</HTML>";



get_footer(); // On affiche de pied de page du thème

?>
