<?php
// calculate_doc_number.php
// Calcul d'un numéro pour le noueau document, et mise à jour du compteur pour ce type de documents
// Version 2019-09-14

//-----------------------------------------------------------
// RECUPERATION VALEUR COMPTEUR POUR CE TYPE DE DOCUMENT
//-----------------------------------------------------------

$requete="SELECT
	prefix,
	doc_number,
	suffix
	FROM type_doc
	WHERE type_doc_id='$type_doc_id' ";

//echo "xx $requete xx<br>";

$result = $dbh->query($requete);
foreach( $result as $row ) {
	$prefix=$row['prefix'];	 // echo "yy $prefix yy";
	$number=$row['doc_number'];	                        
	$suffix=$row['suffix'];
}

//-----------------------------------------------------------
// COMPOSITION DU NUMERO DU NOUVEAU DOCUMENT
//-----------------------------------------------------------

$doc_number=$prefix.str_pad($number+1, 5, "0", STR_PAD_LEFT).$suffix;
//echo "xxx Nouveau numéro : $doc_number xxx";
//-----------------------------------------------------------
// INCREMENTATION DU COMPTEUR POUR CE TYPE DE DOCUMENT
//-----------------------------------------------------------

$requete="UPDATE type_doc
	SET doc_number=doc_number+1
	WHERE type_doc_id='$type_doc_id'
	";
		
// echo "yy $requete yy<br>"; exit;

$stmt= $dbh->prepare($requete);
$stmt->execute();

/*
//-----------------------------------------------------------
// AFFECTATION DU NOUVEAU NUMERO AU NOUVEAU DOCUMENT
//-----------------------------------------------------------

$requete="UPDATE documents
	SET doc_number='$new_doc_number'
	WHERE doc_id='$new_doc_id' ";

 echo "zz $requete zz<br>"; 

$stmt= $dbh->prepare($requete);
$stmt->execute();
*/

?>
