<html>
<?php
// client_doc_upd.php
// Mise à jour des documents client
// Applis MotoClics
// Version 2020-01-14 - Bouzeboudja Reyad

include_once "routines.php";
/*
$client_id=$_REQUEST['client_id']; //  echo "xx $client_id xx<br>"; 
$nouveaunum=$_REQUEST['nouveaunum'];
$doc_number=$_REQUEST['doc_number'];
$transac=$_REQUEST['transac'];
$type_doc_ref=$_REQUEST['type_doc_ref'];
$type_doc_id=$_REQUEST['type_doc_id'];
$doc_template_id=$_REQUEST['doc_template_id'];
*/

include_once "verif_session.php";

include_once "db_connect.php";

include_once "includes/load_soc_info.php";

//echo "zzz $doc_status_code zzz";

if ($transac == "insert" ) {$titrepage="Créer un document client"; }
if ($transac == "update" ) {$titrepage="Modifier un document client"; }

// ------- Recherche statut actuel du document ------

$requete="
SELECT 
doc_status_code,
type_doc_ref
FROM documents 
LEFT JOIN type_doc ON documents.type_doc_id=type_doc.type_doc_id
WHERE doc_id='$doc_id'";
if ($debug == "1") { print "-- $requete --<br>"; }
$result = $dbh->query($requete);
$nb_enr = $result->rowCount();
$d=0;
foreach( $result as $row ) {
        $type_doc_ref =$row['type_doc_ref']; 
        $doc_status_code_old =$row['doc_status_code'];
} # Fin de foreach

// ---------------------------------------------------

echo '<head>';
include_once $_SERVER['DOCUMENT_ROOT']."/programs/includes/style_load.php";
echo "<title>$titrepage</title>";
echo '</head>';
echo '<body>';
echo '<div id=conteneur>';
echo "<div id=header>"; 
include "header.php"; 
echo "</div>";
echo '<div id=frame>';
echo '<center>';

$today = date ("Y-m-j"); 
$now =   date ("Y-m-j H:i:s");


$client_nom=strtoupperFr($client_nom); $client_nom=addslashes($client_nom);
$client_prenom=addslashes($client_prenom);
$client_adresse1=addslashes($client_adresse1);
$client_adresse2=addslashes($client_adresse2);

$client_ville=addslashes($client_ville);
$date_payment=datesys2mysql($date_payment);
$date_delivery=datesys2mysql($date_delivery);
$ref_doc=addslashes($ref_doc); 


// ---------- Recalcul des totaux -------------------------------

if ($taux_remise <> '0') { 
	$total_ht_net= $total_ht * ( (100 - $taux_remise) / 100) ; 
} else {
	$total_ht_net= $total_ht ;
}
$total_ht_net= $total_ht_net + $carriage_ht ; 
$total_tva=$total_ht_net * ( $taux_tva /100) ; 
$net_a_payer=$total_ht_net+$total_tva-$accompte; 
$total_pos_ttc=$total_ht_net+$total_tva;

// ---------------------------------------------------------------
/*
if ($transac == "insert") { // Création du document avec ID client temporaire

	// ----------- Vérification des saisies --------------------------

        if ( $client_id =='' && $client_civil_code == '' ) { $erreur="Veuillez indiquer la civilité du client" ; }
        if ( $client_id =='' && $client_nom =='') { $erreur="Veuillez indiquer le nom du client" ; }
        if ( $erreur <> '' ) {
                echo "<p class=msg_error>Erreur : $erreur </p>";
                print "<INPUT TYPE=button onclick=Javascript:history.go(-1) value=\"";chtext('Retour');  print "\">";
                exit;
	}

        // ------------ Calcul du numéro de document ---------------------
	
	include "calculate_doc_number.php";

	// ------- Chargement ID TVA du document---------
	
	$requete="SELECT taux_tva_id
	FROM taux_tva WHERE taux ='$taux_tva' ";
	# print $requete;
	$result = $dbh->query($requete);
	$nb_enr = $result->rowCount();
	$d=0;
	foreach( $result as $row ) {
        	$taux_tva_id =$row['taux_tva_id'];
        	$d++;
        } # Fin du foreach
	if ($nb_enr=='0' ) { print "<p class=msg_error>Aucun taux de TVA trouve.</p>"; }

	// -----------------------------------------------

	// Client temporaire avant choix de sauvegarder le nouveau client

	if ( $client_id=='' ) { $client_tempo_id='283'; } // Client de passage
	else { $client_tempo_id= $client_id ; }

	$requete="
	INSERT INTO documents 
	(
	type_doc, doc_number, third_id, third_civil_code, third_nom, 
	third_prenom, 
	third_adresse1, third_adresse2, third_codepostal, third_ville,
	doc_tel, doc_mobile, doc_email,
	date_doc, mode_regl, cond_regl, cond_regl_id, 
	doc_template_id,
	taux_remise, interv_id, 
	vehicule_id, vehicule_immatriculation, vehicule_modele, 
	vehicule_kilometrage,
	vehicule_nbHr,
 	vehicule_annee_modele,
	vehicule_serial_number,
	good_color_id, 
	vehicule_manuf_nom, vehicule_manuf_id, vehicule_cnit,
	ref_doc, accompte, doc_status_code, type_doc_id, total_ht_net, carriage_ht, net_a_payer,
	date_debut_asked, date_fin_asked, 
	date_delivery, date_payment, date_cre, user_cre_id) 
	VALUES 
	('$type_doc_ref','$doc_number','$client_tempo_id','$client_civil_code','$client_nom', 
	'$client_prenom',
	'$client_adresse1', '$client_adresse2','$client_codepostal', '$client_ville',
	'$client_tel', '$client_mobile', '$client_email', 
	'$today', '$mode_regl', '$cond_regl', '$cond_regl_id', '$doc_template_id',
	'$taux_remise', '$interv_id', 
	'$vehicule_id', '$vehicule_immatriculation', '$vehicule_modele',";
	if ($vehicule_kilometrage == '') {
		$requete .= " NULL, "; 
	} else {
		$requete .= "'$vehicule_kilometrage',";
	}
	if ($vehicule_nbHr == '') {
		$requete .= " NULL, "; 
	} else {
		$requete .= "'$vehicule_nbHr',";
	}
	if ( $vehicule_annee_modele == '') {
		$requete .= " NULL, "; 
	} else {
		$requete .= "'$vehicule_annee_modele',";
	}
	if ( $vehicule_serial_number == '') {
		$requete .= " NULL, "; 
	} else {
		$requete .= "'$vehicule_serial_number',";
	}
	if ( $good_color_id == '') {
		$requete .= " NULL, "; 
	} else {
		$requete .= "'$good_color_id',";
	}
	$requete .=" 
	'$vehicule_manuf_nom', '$vehicule_manuf_id', '$vehicule_cnit', 
	'$ref_doc', '$accompte','1','$type_doc_id','$total_ht_net', '$carriage_ht', '$net_a_payer', 
	'$now', '$now',
	'$date_delivery','$date_payment', '$now', '$user_id')";

} // Fin du If transac == insert
*/
if ($transac == "update") { // Mise à jour du document
	
	// ------- Recalcul date échéance ----------------
	$requete="
	SELECT
	cond_regl.cond_regl_descr,
	cond_regl.nb_jours,
	cond_regl.fin_de_mois,
	cond_regl.jour_du_mois
	FROM cond_regl
	WHERE cond_regl_id='$cond_regl_id'";
	if ($debug == "1") { print "-- $requete --<br>"; }
	$result = $dbh->query($requete);
	$nb_enr = $result->rowCount();
	$d=0;
	foreach( $result as $row ) {
	     $cond_regl_descr =$row['cond_regl_descr'];
	     $nb_jours =$row['nb_jours'];
	     $fin_de_mois =$row['fin_de_mois'];
	     $jour_du_mois =$row['jour_du_mois'];
	     // echo "Condition de règlement : $cond_regl_descr Nbre de jours : $nb_jours Jour du mois : $jour_du_mois <br>";
    	} # Fin de foreach
	//if ($nb_enr == 0) { print "Totaux du document non trouvés"; }
//	include "echeance_calcul2.php";
//	$date_payment = CalculTraite($date_doc, $nb_jours, $fin_de_mois, $jour_du_mois );
//	echo "la date est : $date_payment";
        // ------ Fin Recalcul date écheance ----------------

	// ------- Chargement totaux actuels du document -----

	$requete="
	SELECT total_ht, total_ht_net, total_pos_ttc, net_a_payer 
	FROM documents 
	WHERE doc_id='$doc_id'";
	if ($debug == "1") { print "-- $requete --<br>"; }
	$result = $dbh->query($requete);
	$nb_enr = $result->rowCount();
	$d=0;
	foreach( $result as $row ) {
		$total_ht =$row['total_ht'];
		$total_ht_net =$row['total_ht_net'];
		//$total_tva =$row['total_tva'];
		$total_pos_ttc =$row['total_pos_ttc'];
		$net_a_payer =$row['net_a_payer'];
	} # Fin du foreach
	if ($nb_enr == 0) { print "Totaux du document non trouvés"; }
	
	// ---------------------------------------------------
	
	// ------- Mise à jour du document -------------------

	$requete="UPDATE documents SET
	doc_status_code='$doc_status_code',
	date_modif='$now',user_modif_id='$user_id'
	WHERE doc_id='$doc_id'";

//echo "xx Requete update document $requete xx<br>";

} // Fin du If transac == update

// -------- Execution de la requete d'insertion ou de mise à jour du document construite plus haut ------------------------

if ($debug == "1") { print "-- $requete --<br>"; }

$stmt= $dbh->prepare($requete);
$stmt->execute();
$new_doc_id =  $dbh->lastInsertId();

// ------------------------------------------------------------------------------------------------------------------------


// Si le document est une facture client et que le statut passe de "Ouvert" à "Confirmé" ou "Soldé"
// alors sortie de stock des pièces et changement de propriétaire pour les véhicules 
if (($type_doc_ref == "FACTCLI" || $type_doc_ref == "BRVO") && $doc_status_code_old == '1' && ( $doc_status_code=='2' || $doc_status_code=='4') )
	{
	 include "doc_stock_sortie.php"; 
	 include "client_doc_vehicule_change_owner.php"; 
	 include "livre_police_insert.php"; 
	}

// si on a un document modèle, alors on copie ses lignes
if ( $transac =='insert' && $doc_template_id <>'' )
        { include "doc_lines_copy.php"; }
/*
if ( $transac =='insert' || $transac == 'update' ) { // Création fiche client ou MAJ fiche client & vehicule
	if ( $client_id =='' ) {
		
		echo "<form action=client_doc_crecli.php?session_id=$session_id&lang=$lang>";
		echo "<input type=hidden name=session_id value =$session_id>";
		echo "<input type=hidden name=lang value =$lang>";
		echo "<input type=hidden name=doc_id value =$new_doc_id>";
		echo "<input type=hidden name=doc_template_id value =$doc_template_id>";
		echo "<input type=hidden name=client_id value =$client_id>";
		echo "<input type=hidden name=client_civil_code value =$client_civil_code>";
		echo "<input type=hidden name=client_nom value =\"$client_nom\">";
		echo "<input type=hidden name=client_prenom value =\"$client_prenom\">";
		echo "<input type=hidden name=client_adresse1 value =\"$client_adresse1\">";
		echo "<input type=hidden name=client_adresse2 value =\"$client_adresse2\">";
		echo "<input type=hidden name=client_codepostal value =\"$client_codepostal\">";
		echo "<input type=hidden name=client_ville value =\"$client_ville\">";
		echo "<input type=hidden name=client_tel value =\"$client_tel\">";
		echo "<input type=hidden name=client_mobile value =\"$client_mobile\">";
		echo "<input type=hidden name=client_email value =\"$client_email\">";
		echo "<input type=hidden name=new_vehicule_id value =\"$new_vehicule_id\">";
		echo "<h2 class=center>Voulez-vous enregistrer le nouveau client ?</h2>";
		echo "<input id='oui' type='submit' value='Oui' />";
		?><script>document.getElementById("oui").focus();</script><?php
		echo "$nbsp$nbsp";
		echo "<input type=button name='non' value='Non' onclick=\"document.location='client_doc_modif.php?session_id=$session_id&lang=$lang&doc_id=$new_doc_id'\" />";
		echo "</form>";
	} else { 

		// ---- Comparaison fiche CLIENT et document -------------------

		try {
			$requete="SELECT
				civil_code,
				nom,
				prenom,
				adresse1,
				adresse2,
				codepostal,
				ville,
				tel,
				mobile,
				email
				FROM
				persons
				WHERE
				person_id='$client_id';";

			$result=$dbh->query($requete);
			$client_old=$result->fetch(PDO::FETCH_ASSOC);
			$client_new=[
				'civil_code' => $client_civil_code,
				'nom' => $client_nom,
				'prenom' => $client_prenom,
				'adresse1' => $client_adresse1,
				'adresse2' => $client_adresse2,
				'codepostal' => $client_codepostal,
				'ville' => $client_ville,
				'tel' => $client_tel,
				'mobile' => $client_mobile,
				'email' => $client_email
			];
			foreach ($client_new as $key => $value) {
				$client_new[$key] = stripslashes($value);
			}
			$i = 0;
			foreach ($client_old AS $key => $value) {
				if ($value <> $client_new[$key]) {
					$difference_client[$i]['field']=$key;
					$difference_client[$i]['old']=$client_old[$key];
					$difference_client[$i]['new']=$client_new[$key];
					$i++;
				}
			}	

		} catch (exception $e) {
			echo "<p class=msg_error >Erreur comparaison fiche client : ".$e->getMessage()."</p>";
			exit;
		}
//echo $vehicule_id;
		// ---- Comparaison fiche VEHICULE et document -------------------

		try {
			$requete="SELECT
			manuf_id,
			vehicule_modele,
			vehicule_kilometrage,
			vehicule_nbHr,
			vehicule_annee_modele,
			vehicule_immatriculation,
			vehicule_cnit,
			serial_number,
			good_color_id
			FROM goods
			WHERE good_id='$vehicule_id';";

			$result=$dbh->query($requete);
			$vehicule_old=$result->fetch(PDO::FETCH_ASSOC);
			$vehicule_new=[
				'manuf_id' => $vehicule_manuf_id,
				'vehicule_immatriculation' => $vehicule_immatriculation,
				'vehicule_modele' => $vehicule_modele,
				'vehicule_annee_modele' => $vehicule_annee_modele,
				'vehicule_cnit' => $vehicule_cnit,
				'vehicule_kilometrage' => $vehicule_kilometrage,
				'vehicule_nbHr'=> $vehicule_nbHr,
				'serial_number' => $vehicule_serial_number,
				'good_color_id' => $good_color_id
			];
			

			foreach ($vehicule_new as $key => $value) {
				$vehicule_new[$key] = stripslashes($value);
			}

			$i = 0;
			foreach ($vehicule_old AS $key => $value) {
				if ($value <> $vehicule_new[$key]) {
					$difference_vehicule[$i]['field']=$key;
					$difference_vehicule[$i]['old']=$vehicule_old[$key];
					$difference_vehicule[$i]['new']=$vehicule_new[$key];
					$i++;
				}
			}
			

		} catch (exception $e) {
			echo "<p class=msg_error >Erreur comparaison fiche Véhicule : ".$e->getMessage()."</p>";
			exit;
		}	

		// ------ Formulaire de choix de mise à jour des fiches client & véhicule -----------------------

		$doc_id = $doc_id ?: $new_doc_id;

		if ($difference_client || $difference_vehicule) {
			
			
			echo "<button onclick=\"history.go(-1);\">";c('Retour');echo"</button>"; 
		
			echo "<form action=client_doc_maj_cli_veh.php?session_id=$session_id&lang=$lang>";
			echo "<input type=hidden name=session_id value =$session_id>";
			echo "<input type=hidden name=lang value =$lang>";
			echo "<input type=hidden name=doc_id value =$doc_id>";
			echo "<input type=hidden name=client_id value =$client_id>";
			echo "<input type=hidden name=client_civil_code value =$client_civil_code>";
			echo "<input type=hidden name=client_nom value =\"$client_nom\">";
			echo "<input type=hidden name=client_prenom value =\"$client_prenom\">";
			echo "<input type=hidden name=client_adresse1 value =\"$client_adresse1\">";
			echo "<input type=hidden name=client_adresse2 value =\"$client_adresse2\">";
			echo "<input type=hidden name=client_codepostal value =\"$client_codepostal\">";
			echo "<input type=hidden name=client_ville value =\"$client_ville\">";
			echo "<input type=hidden name=client_tel value =\"$client_tel\">";
			echo "<input type=hidden name=client_mobile value =\"$client_mobile\">";
			echo "<input type=hidden name=client_email value =\"$client_email\">";		
			echo "<input type=hidden name=vehicule_id value =\"$vehicule_id\">";
			echo "<input type=hidden name=vehicule_manuf_id value =\"$vehicule_manuf_id\">";
			echo "<input type=hidden name=vehicule_immatriculation value =\"$vehicule_immatriculation\">";
			echo "<input type=hidden name=vehicule_modele value =\"$vehicule_modele\">";
			echo "<input type=hidden name=vehicule_annee_modele value =\"$vehicule_annee_modele\">";
			echo "<input type=hidden name=vehicule_cnit value =\"$vehicule_cnit\">";
			echo "<input type=hidden name=vehicule_kilometrage value =\"$vehicule_kilometrage\">";
			echo "<input type=hidden name=vehicule_nbHr value =\"$vehicule_nbHr\">";
			echo "<input type=hidden name=vehicule_serial_number value =\"$vehicule_serial_number\">";
			echo "<input type=hidden name=good_color_id value =\"$good_color_id\">";

			// ---- Tableaux récapitulatifs des différences entre fiche et document -------------------	
			
			echo "<div style='display : inline-block;' >";

			if ($difference_client) {
				echo "<div style='display : inline-block; margin : 1em;' >";
				echo "<p>Mettre à jour la fiche client ?</p>";
				echo "<table class=choix>";
				echo "<tr style='background-color : #F99;'>";
				echo "<th>";chtext('Champ');echo "</th>";
				echo "<th>";chtext('Fiche Client');echo "</th>";
				echo "<th>";chtext('Document actuel');echo "</th>";
				echo "</tr>";
				$i = 0;
				foreach ($difference_client as $key => $value) {
					//var_dump($difference_client);
					if ($i%2 == 0) { $bg_color = "white"; }
					else { $bg_color = "#FDD"; }
					echo "<tr>";
					echo "<td style='background-color : $bg_color; font-weight : bold; color : #333;' >".$difference_client[$i]['field']."</td>";
					echo "<td style='background-color : $bg_color;' >".$difference_client[$i]['old']."</td>";
					echo "<td style='background-color : $bg_color;' >".$difference_client[$i]['new']."</td>";
					echo "</tr>";
					$i++;
				}
				echo "</table>";
				 echo "<p>";
				echo "<label for=oui_maj_client>";
				chtext('Oui');
				echo"</label><input style='margin-right : 2em;' checked id=oui_maj_client type=radio name=maj_client value=true />";
				echo "<label for=non_maj_client>";
				chtext('Non');
				echo"</label><input id=non_maj_client type=radio name=maj_client value=false />";
				echo "</p>";
				echo "</div>";
			}
		
				
			if ($difference_vehicule) {
				echo "<div style='display : inline-block; vertical-align:top; margin : 1em;' >";
				echo "<p>Mettre à jour la fiche véhicule ?</p>";
				echo "<table class=choix>";
				echo "<tr style='background-color : #99F;'>";
				echo "<th>";chtext('Champ');echo "</th>";
				echo "<th>";chtext('Fiche Véhicule');echo "</th>";
				echo "<th>";chtext('Document actuel');echo "</th>";
				echo "</tr>";
				$i = 0;
				foreach ($difference_vehicule as $key => $value) {
					//var_dump($difference_vehicule);
					if ($i%2 == 0) { $bg_color = "white"; }
					else { $bg_color = "#DDF"; }
					echo "<tr>";
					echo "<td style='background-color : $bg_color; font-weight : bold; color : #333;' >".$difference_vehicule[$i]['field']."</td>";
					echo "<td style='background-color : $bg_color;' >".$difference_vehicule[$i]['old']."</td>";
					echo "<td style='background-color : $bg_color;' >".$difference_vehicule[$i]['new']."</td>";
					echo "</tr>";
					$i++;

				}
				if ($vehicule_kilometrage < $vehicule_old[vehicule_kilometrage] )
				{
					if ($vehicule_kilometrage != '')
					{
					echo "<p class=msg_warning>Attention le kilométrage ($vehicule_kilometrage) saisi est inférieur au kilométrage déjà saisi ($vehicule_old[vehicule_kilometrage]) pour ce véhicule<p>";
					}
				}
				if ($vehicule_kilometrage == 0 || $vehicule_kilometrage == '')
				{
					echo "<p class=msg_warning>Attention, vous n'avez pas saisi de kilométrage ou celui-ci est égal à 0<p>";
				}
				if ($vehicule_nbHr < $vehicule_old[vehicule_nbHr] )
				{
					if ($vehicule_nbHr != '')
					{
					echo "<p class=msg_warning>Attention le nombre d'heures de fonctionnement ($vehicule_nbHr) saisi est inférieur au nombre d'heures de fonctionnement déjà saisi ($vehicule_old[vehicule_nbHr]) pour ce véhicule<p>";
					}
				}
				if ($vehicule_nbHr == 0 || $vehicule_nbHr == '')
				{
					echo "<p class=msg_warning>Attention, vous n'avez pas saisi de nombre d'heures de fonctionnement ou celui-ci est égal à 0<p>";
				}
				echo "</table>";
				echo "<p>";
				echo "<label for=oui_maj_vehicule>";
				chtext('Oui');
				echo"</label><input style='margin-right : 2em;' checked id=oui_maj_vehicule type=radio name=maj_vehicule value=true />";
				echo "<label for=non_maj_vehicule>";
				chtext('Non');
				echo"</label><input id=non_maj_vehicule type=radio name=maj_vehicule value=false />";
				echo "</p>";
				echo "</div>";
			}	

			echo "</div>";
			echo "<p><input autofocus type=submit /></p>";
			echo "</form>";
		} else { // Si il n'y pas de différences entre les fiches et le document
			print "<meta http-equiv=refresh content=0;url=client_doc_modif.php?doc_id=$doc_id&lang=$lang&session_id=$session_id />";
		}
	}
} // Fin du If transac == insert ou update

if ($transac == "delete") { 
	print "<meta http-equiv=refresh content=0;url=client_doc_choix.php?lang=$lang&session_id=$session_id />"; 
}
*/
print "</div>";

print "</div>";
?>
</BODY>
</HTML>
