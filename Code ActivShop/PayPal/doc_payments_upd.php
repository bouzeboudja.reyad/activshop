<?php
// doc_payments_upd.php
 // Mise à jour d'un paiement
// Applis ActiVgest, MotoClic, RestoClic
// Version 2021-04-28

include_once 'routines.php';

$now = date ("Y-m-j H:i:s");

include_once $_SERVER['DOCUMENT_ROOT'].'/programs/verif_session.php';

include_once $_SERVER['DOCUMENT_ROOT'].'/programs/db_connect.php';

if ( $doc_payments_transac== 'insert' && $mode_regl_id =='' ) 
	{ $error="Veuillez préciser le mode de paiement"; }
if ( $doc_payments_transac== 'insert' && ( $payment_amount =='0' || $payment_amount == '' ) ) 
	{ $error="Veuillez préciser le montant du paiement"; }
/*if ( $error <> '' )
	{ 
	print "$error<br>";
	print "<INPUT TYPE=button onclick=Javascript:history.go(-1) value=\"";chtext('Retour');  print "\">"; 
	exit;
	}
*/
// --- Récupération du compte comptable du mode de règlement ---
$requete="SELECT financial_account_number FROM mode_regl WHERE mode_regl_id='$mode_regl_id'";

$result = $dbh->query($requete);
$nb_enr = $result->rowCount();
$d=0;
foreach( $result as $row ) {
        $financial_account_number =$row['financial_account_number'];
} # Fin de foreach

// echo "xx $requete - $client_id xx"; exit;
// echo "xx $financial_account_number xx<br>"; exit;
// --- Fin Récupération du compte comptable du mode de règlement ---

// --- Récupération de l'ID du client et du type de document dans le document ---
$requete="SELECT 
third_id,
documents.doc_status_code,
documents.doc_number, 
documents.type_doc_id,
documents.affair_id,
type_doc.type_doc_ref,
type_doc.type_doc_descr
FROM documents 
LEFT JOIN type_doc on documents.type_doc_id=type_doc.type_doc_id
WHERE doc_id='$doc_id'";
$result = $dbh->query($requete);
$nb_enr = $result->rowCount();
$d=0;
foreach( $result as $row ) {
        $master_doc_number =$row['doc_number'];
        $master_type_doc_ref =$row['type_doc_ref'];
        $master_type_doc_descr =$row['type_doc_descr'];
        $affair_id =$row['affair_id'];
        $client_id =$row['third_id'];
        $type_doc_id =$row['type_doc_id'];
	$doc_status_code =$row['doc_status_code'];
	$old_doc_status_code =$row['doc_status_code'];
} # Fin de foreach

// echo "xx $requete xx";
// - $master_type_doc_descr xx"; exit;
// --- Fin Récupération de l'ID du client dans le document ---


$date_payment=datesys2mysql($date_payment);

if ( $doc_payments_transac== 'insert' ) {
	$requete="
	INSERT INTO doc_payments
	( 
	doc_id, acompte, date_payment, mode_regl_id, payment_amount, third_id, financial_account_number,
	affair_id,
	date_cre, user_cre_id
	)
	VALUES 
	(
	'$doc_id', '$acompte', '$date_payment', '$mode_regl_id', '$payment_amount', '$client_id', '$financial_account_number',
	'$affair_id',
	'$now', '$user_id'
	)
	";
}

if ( $doc_payments_transac== 'delete' ) {
        $requete="
	DELETE FROM doc_payments
	WHERE doc_payment_id='$doc_payment_id'
	";
}

if ($debug == "1") { print "-- $requete --<br>"; }
// echo "-- $requete --<br>";
// exit;

$stmt= $dbh->prepare($requete);
$stmt->execute();
$doc_payment_id = $dbh->lastInsertId();

// Si ce paiement est un acompte, alors on créé une facture d'acompte 
if ( $acompte=='O' ) { include 'factcli_acompte_prepa.php'; }


if ($type_doc_id == 2 || $type_doc_id == 13) {

	// On passe le document en statut "Soldé" si le total TTC = le total payé
	$requete="
	SELECT
	documents.total_pos_ttc,
	sum(doc_payments.payment_amount) AS total_payment_amount
	FROM
	documents LEFT JOIN doc_payments ON documents.affair_id=doc_payments.affair_id
	WHERE documents.doc_id='$doc_id'
	";

	$stmt=$dbh->prepare($requete);
	$stmt->execute();
	$result=$stmt->fetch(PDO::FETCH_ASSOC);
	
	$total_payment_amount=$result['total_payment_amount'];
	$total_pos_ttc=$result['total_pos_ttc'];

	if ( $total_payment_amount == $total_pos_ttc )	{

		$requete="UPDATE documents SET doc_status_code='4' WHERE doc_id='$doc_id'";
		$dbh->query($requete);
		if ($old_doc_status_code == '1') {
			include "doc_stock_sortie.php";
			include "client_doc_vehicule_change_owner.php";
			include "livre_police_insert.php";
		}
	}
}

?>

