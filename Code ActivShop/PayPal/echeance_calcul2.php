<?php
// Version 2015-05-05
function CalculTraite($DateDepart, $NbJours, $FinDeMois, $JourMoisSuivant)
{
// Calcule la date d'une traite (échéance)
// Ex : $TimeStamp = CalculTraite('01-08-2007', 90, 15);

$echeance=$DateDepart;

// --------- On ajoute le nombre de jours ----------------
if ( $NbJours <>'' && $NbJours <>'0' )
	{
	$echeance = date('Y-m-d', strtotime($echeance . " + $NbJours day"));
	}
// echo "Echéance après nb jours ( $NbJours ) : $echeance <br>"; 

// --------- Si Fin de mois, On saute au dernier jour ----------------
if ( $FinDeMois =='o' )
	{
	$NbJourMois=cal_days_in_month(CAL_GREGORIAN, substr($echeance,5,2), substr($echeance,0,4)); 
	// echo "xx Nombre de jours du mois : $NbJourMois xx<br>";
	//$NbJourMois = date("t", $echeance);
	$echeance=substr($echeance,0,4)."-".substr($echeance,5,2)."-".$NbJourMois;
	}
// echo "Echéance après fin de de mois : $echeance <br>"; 

// echo "Jour mois suivant:  $JourMoisSuivant <br>";

// --------- Si Fin de Mois le xx, On saute au dernier jour ----------------
if ( $JourMoisSuivant <>'' && $JourMoisSuivant <> '0' )
	{
	// On avance d'un mois
	$annee=substr($echeance,0,4);
	$mois= substr($echeance,5,2);
	// Formule qui tient compte du mois de février
	$echeance=date('Y-m-t',strtotime("$annee-$mois-31T23:59:59 -3 days +1 month"));
	// echo "Echéance après avancée d'un mois : $echeance <br>";
	// On revient en arrière le jour du mois voulu
	$echeance=substr($echeance,0,4)."-".substr($echeance,5,2)."-".$JourMoisSuivant;
	}
//echo "Echéance après jour du mois suivant: $echeance <br>";

// exit;

return $echeance;
}
?>
