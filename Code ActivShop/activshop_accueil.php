<?php
/*
Template Name: activshop_accueil
*/
// Boutique en lgne : Choix des produits
// Version 2021-02-24
get_header(); // On affiche l'en-tête du thème WordPress

include 'routines.php';

include 'activshop_style.css';

$session_id=$_REQUEST[session_id]; // echo "ss Session : $session_id ss<br>";
$doc_id=$_REQUEST[doc_id];

//Début DIV PARTIE HAUTE (CARTOUCHE + WIDGETS)
echo "<div style=\"display : flex; flex-direction : row; justify-content : space-between;\">";

// Début DIV CARTOUCHE DOCUMENT
echo "<div style=\"display : inline-block\">";

echo '<fieldset>';
echo "<form method=GET onsubmit='/recherche-produit'>";
echo "<input type=hidden name=session_id value=$session_id>";
echo "<input type=hidden name=doc_id value=$doc_id>";
echo "<table style=width:800px class=trans>";

echo '<tr>';
// ---------------Sélection référence --------------------
echo "<td>Produit<input class=search type=text name=recherche id=recherche value=\"$recherche\"></td>";
// ----------------------------------------------------------

echo '</tr>';
echo "</table>";

echo "<center><input  name=update type=submit id=update value=Rechercher></center>";
echo '</fieldset>';

echo "</div>";
// Fin DIV CARTOUCHE DOCUMENT

// Début DIV WIDGETS
echo "<div style=\"display : flex; flex-direction : column; justify-content:space-between; title=\"Voir le panier\" \">";

// --- Début DIV panier ---
if ( $doc_id <>'' ) { $onclick='onclick'; }
echo "<div class=panier $onclick=\"location.href='activshop-panier-visu?session_id=$session_id&doc_id=$doc_id';\" title=\"Voir le panier\" >";
echo "<center><img style=width:40px src=/images/activshop_panier_logo.png>";
$requete="SELECT doc_number, COUNT(doc_pos_id) AS nb_lignes
FROM documents LEFT JOIN doc_positions ON documents.doc_id=doc_positions.doc_id
WHERE documents.doc_id='$doc_id'
";
$paniers = $wpdb->get_results($requete);
foreach($paniers as $panier) {
    echo '<div>'.$panier->doc_number.'</div>';
    echo '<div class=numberCircle>'.$panier->nb_lignes.'</div>';
}
echo '</div>'; 
// --- Fin DIV Panier ---

echo '</div>'; // Fin DIV WIDGETS

echo '</div>'; // Fin DIV PARTIE HAUTE (DOCUMENT + WIDGETS)


get_footer(); // On affiche de pied de page du thème

?>

