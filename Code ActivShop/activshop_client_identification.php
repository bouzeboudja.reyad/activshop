<?php
/*
Template Name: activshop_client_identification
*/
// Version 2021-05-06
get_header(); // On affiche l'en-tête du thème WordPress

include 'routines.php';

include 'activshop_style.css';

$session_id=$_REQUEST[session_id]; // echo "ss Session : $session_id ss<br>";
$doc_id=$_REQUEST[doc_id];
$client_id=$_REQUEST[client_id];

// --- Affichage Numéro de la commande (panier) ---
$requete="
SELECT third_id, doc_number 
FROM documents
WHERE doc_id='$doc_id';
";
$result = $wpdb->get_results($requete);
foreach ($result AS $row ) {
      $doc_number= $row->doc_number; 
      if ( $client_id =='' ) { $client_id= $row->third_id; /* echo "xx $client_id xx"; */ }
}
// echo "xx $client_id xx";
echo "<h1>Commande n° $doc_number</h1>";
// --- Fin Affichage Numéro de la commande (panier) ---


// --- Si n° de client renseigné --------
// --- Recherche du client dans la base de données ---
$encrypted_password=md5($password);
$requete="
SELECT
person_id, civil_code, nom, prenom, adresse1, adresse2, codepostal, ville, country, tel, email
FROM persons WHERE person_id='$client_id'
";
// echo "xx $requete xx<br>";

$result = $wpdb->get_results($requete);
// var_dump($result);
foreach ($result AS $row ) {
      $client_id= $row->person_id;
      $civil_code= $row->civil_code;
      $nom= $row->nom;
      $prenom= $row->prenom;
      $adresse1= $row->adresse1;
      $adresse2= $row->adresse2;
      $codepostal= $row->codepostal;
      $ville= $row->ville;
      $country= $row->country;
      $tel= $row->tel;
      $email= $row->email;
}
// --- Fin Recherche du client dans la base de données ---



include 'activshop_ariane.php';

//Début DIV PARTIE HAUTE (CARTOUCHE + WIDGETS)
echo "<div style=\"display : flex; flex-direction : row; justify-content : space-between;\">";


// Début DIV CARTOUCHE DOCUMENT
echo "<div style=\"display : inline-block\">";

echo '<fieldset style=color:black;>';
echo '<center><h2>';
if ( $client_id =='' || $client_id =='283' ) { echo 'Pas encore client ? Inscrivez-vous'; }
else { echo 'Merci de vérifier vos coordonnées ci-dessous'; }
echo '</h2>';
echo "<form method=POST action=/activshop-client-update>";
echo "<input type=hidden name=session_id value=$session_id>";
echo "<input type=hidden name=doc_id value=$doc_id>";
echo "<input type=hidden name=transac value=insert >";
echo "<table style=width:800px class=trans>";

echo '<tr>';
echo '<td>';
// ------- Chargement liste des civilités -----
echo "Civilité<br><select style=width:50% required name=civil_code id=civil_code >";
$requete="
SELECT DISTINCT civil_code 
FROM civilites 
";

$liste_civil = $wpdb->get_results($requete);
foreach ($liste_civil AS $civil ) {
	if ( $manuf_id == $manuf->person_id ) { $manuf_nom= $manuf->nom; }
}
echo "<option value=".$civil_code.">".$civil_code;
foreach ($liste_civil AS $civil ) {
	echo "<option value=".$civil->civil_code.">".$civil->civil_code;
}
echo '</select>';
// -------------------------- Saisie Nom --------------------
echo "<td>Nom<input required type=text name=nom id=nom value=\"$nom\"></td>";
// -------------------- Saisie Prénom --------------------
echo "<td>Prénom<input type=text name=prenom id=prenom value=\"$prenom\"></td>";
// ----------------------------------------------------------
echo "</tr><tr>";
echo "<td>Adresse ligne 1<input required type=text name=adresse1 id=adresse1 value=\"$adresse1\"></td>";
// ----------------------------------------------------------
echo "<td>Adresse ligne 2<input type=text name=adresse2 id=adresse2 value=\"$adresse2\"></td>";
// ----------------------------------------------------------
echo "</tr><tr>";
echo "<td>Code Postal<br><input type=text style=width:50% required name=codepostal id=codepostal value=\"$codepostal\"></td>";
echo "<td>Ville<input type=text required name=ville id=ville value=\"$ville\"></td>";

echo "</tr><tr>";

// ------- Chargement liste des Pays -----
echo '<td>';
echo "Pays<br><select required name=country id=country >";
$country_name='France';
$requete="
SELECT DISTINCT country_name
FROM countries 
";

$liste_countries = $wpdb->get_results($requete);
foreach ($liste_countries AS $country ) {
	if ( $manuf_id == $manuf->person_id ) { $manuf_nom= $manuf->nom; }
}
echo "<option value=".$country_name.">".$country_name;
foreach ($liste_countries AS $country ) {
	echo "<option value=".$country->country_name.">".$country->country_name;
}
echo '</select>';
echo '</td>';
// ----------------------------------------------------------

echo '<tr>';
// ----------------------------------------------------------
echo "<td>Téléphone<input required type=text name=tel id=tel value=\"$tel\"></td>";
// ----------------------------------------------------------
echo "<td>Email<input required type=text name=email id=email value=\"$email\"></td>";

?>

<script type="text/javascript">

var check = function()
{
 if (document.getElementById('password').value == document.getElementById('confirm_password').value) 
 {
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'Mot de passe identique';
    document.getElementById('update').disabled = false;
 }
 else
 {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'Mot de passe différent';
    document.getElementById('update').disabled = true;
 }
}
</script>

<?php
if ( $client_id =='' || $client_id =='283' )
{
echo "<td>Mot de Passe<input required name=password id=password type=password value=\"$password\" onkeyup=check();></td>";
echo "<br>";
echo "<td>Confirmer le Mot de passe :<input required type=password name=confirm_password id=confirm_password value=\"$confirm_password\" onkeyup=check();><span id=message></span></td>";
}
echo '</tr>';
echo "</table>";
echo "<center><input  name=update type=submit id=update value=Confirmer></center>";
echo '</form>';
echo '</fieldset>';
echo "</div>";
// Fin DIV CARTOUCHE DOCUMENT

// Début DIV WIDGETS
echo "<div style=\"display : flex; flex-direction : column; justify-content:space-between;  \">";
if ( $client_id =='' || $client_id =='283' ) {
echo "<div>"; // --- Début DIV déja client ---

echo '<fieldset style=color:black;>';
echo "<center><h2>Déjà client ? Authentifiez-vous</h2>";
echo "<form method=POST action=/activshop-client-authentification>";
echo "<input type=hidden name=session_id value=$session_id>";
echo "<input type=hidden name=doc_id value=$doc_id>";
echo "<table class=trans>";
echo '<tr>';
// ----------------------------------------------------------
echo "<td>Email<br><input style=width:50 required type=text name=email id=email value=\"$email\"></td>";
echo '</tr>';
echo '<tr>';
// ----------------------------------------------------------
echo "<td>Mot de passe<br><input style=width:50 required type=password name=password id=password value=\"$password\"></td>";

echo '</tr>';

echo '<tr>';
echo "<td><a href=pass-oublie>Mot de passe oublié ?</a></td>";
echo '</tr>';
echo '</table>';
echo "<center><input  name=update type=submit id=update value=Confirmer></center>";
echo '</form>';

echo '</fieldset>';
echo "</div>"; // Fin DIV Panier
}

echo "</div>"; // Fin DIV WIDGETS

echo "</div>"; // Fin DIV PARTIE HAUTE (DOCUMENT + WIDGETS)

?>

<?php get_footer(); // On affiche de pied de page du thème
?>
