<HTML>
<?php
// activshop_client_upd.php
// Mise à jour du client
// Applis : ActivShop
// Version 2021-05-06
/*
Template Name: activshop_client_upd
*/

get_header(); // On affiche l'en-tête du thème WordPress

// On enl▒ve les antislashes en trop
$_POST = wp_unslash( $_POST );

include 'activshop_style.css';

include 'instance_conf.php';

include 'db_connect.php';

$session_id=$_POST['session_id'];  //echo "ss $session_id ss<br>";
$doc_id=$_POST['doc_id'];   //echo "dd DOC ID : $doc_id dd<br>";
$civil_code=$_POST['civil_code'];  //echo "cc $civil_code cc";
$nom=$_POST['nom'];
$prenom=$_POST['prenom'];
$adresse1=$_POST['adresse1'];
$adresse2=$_POST['adresse2'];
$codepostal=$_POST['codepostal'];
$ville=$_POST['ville'];
$country=$_POST['country'];
$tel=$_POST['tel'];
$email=$_POST['email'];
$password=$_POST['password'];
$encrypted_password=md5($password);
$transac=$_POST['transac'];  //echo "xx $transac xx";



$cli = "O";
$date_jour= date ("Y-m-d");
$now = date ("Y-m-d H:i:s");

print "<body>";
print "<div id=conteneur>";

print "<div id=frame>";
echo '<center>';

// ----------- Vérification des saisies ---------------
//if ( $password <> $password2 ) {$erreur="Les mots de passe ne correspondent pas";}
if ( $transac=='insert' || $transac=='update' ) {
        if ( $nom == '' ) {$erreur="Vous devez indiquer votre nom";}
}
// --------Fin vérification des saisies ---------------
                
global $wpdb; // On se connecte à la base de données du site

if ( $erreur <> '')
        {
        print "<font color=red><B>Fiche incomplete </B></font>$erreur";
        //print "<INPUT TYPE=button onclick=Javascript:history.go(-1) value=\"";chtext('Retour'); print "\"";
        #<!--FIN CODE PAGE PRECEDENTE-->
        }
else
        {

        // ---- Insertion ----
        if ( $transac == "insert" ) {
		// echo "INSERTION<BR>";

		// ---- On crée la fiche du client  ----
                    $wpdb->insert("persons",array(
		    "civil_code"=>$civil_code,
		    "nom"=>$nom,
		    "prenom"=>$prenom,
		    "adresse1"=>$adresse1,
		    "adresse2"=>$adresse2,
		    "codepostal"=>$codepostal,
		    "ville"=>$ville,
		    "country"=>$country,
		    "tel"=>$tel,
		    "email"=>$email,
                    "password"=>$encrypted_password,
		    "cli"=>$cli,
		    "date_cre"=>$now
		    ));
		$client_id = $wpdb->insert_id;
//                 $wpdb->show_errors();
  //               $wpdb->print_error();
		// ---- Fin de On crée la fiche du client  ----


		// --- On associe le nouveau client au document ------
		$wpdb->update("documents",array(
                "third_civil_code" => $civil_code,
                "third_id" => $client_id,
                "third_nom" => $nom,
                "third_prenom" => $prenom,
                "third_adresse1" => $adresse1,
                "third_adresse2" => $adresse2,
                "third_codepostal" => $codepostal,
                "third_ville" => $ville,
                "third_ville" => $ville,
                'pays_doc'=>$country,
                'doc_tel'=>$tel,
                'doc_email'=>$email
                ),
                array(
                'doc_id'=> $doc_id
                )
                );
		// ---Fin de On associe le nouveau client au document ------




	} // Fin de insert

         if ( $transac == "update" ) {

                $wpdb->update("persons",array(
                "date_modif" => $now,
                "pos_qte"=>$quantite
                ),
                array(
                "doc_pos_id"=> $doc_pos_id
                )
                );

                //$wpdb->print_error();

         } // Fin de update

        if ( $transac == "delete" ) {

                $wpdb->delete("doc_positions",array(
                "doc_pos_id"=> $doc_pos_id
                )
                );

   		// $wpdb->print_error();

        } // Fin de delete
}

// print "<center><br><a href=\"/produits/?session_id=$session_id&doc_id=$doc_id\" class=button>Retour</a></center>";
print "<meta http-equiv=refresh content=0;url=/activshop-paiement/?session_id=$session_id&doc_id=$doc_id />";

//print "<center><br><INPUT TYPE=button onClick=\"parent.location='liste-des-entreprises'\" VALUE='Retour'></center>";

//        print "<center><p class=msg_ok>L'entreprise a bien été enregistré</p></center>";
        /*if ( $transac == 'update' )
                {
                print "<meta http-equiv=refresh content=0;url=clients_modif.php?person_id=$person_id&lang=$lang&session_id=$session_id />";
                }
        else
                { print "<meta http-equiv=refresh content=0;url=clients_choix.php?type=client&person_id=$new_person_id&lang=$lang&session_id=$session_id />"; }

        print "</CENTER>";
        }*/
print "</div>";

print "</div>";
?>
</BODY>
</HTML>

<?php  get_footer(); // On affiche de pied de page du thème
?>

