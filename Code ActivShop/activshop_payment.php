<?php
/*
Template Name: activshop_payment
*/
// Version 2020-12-30
get_header(); // On affiche l'en-tête du thème WordPress

include 'routines.php';

include 'activshop_style.css';

$session_id=$_REQUEST[session_id]; // echo "ss Session : $session_id ss<br>";
$doc_id=$_REQUEST[doc_id];
$client_id=$_REQUEST[client_id];
$delivery_mode_cocher=$_REQUEST['delivery_mode_cocher'];
$delivery_mode_cocher_price=$_REQUEST['delivery_mode_cocher_price'];
$total=$_REQUEST[total];

//echo "oeoeoe $delivery_mode_cocher et $delivery_mode_cocher_price eoeooe";


include 'activshop_ariane.php';

echo "<h1>Récapitulatif de votre commande</h1>";

// ------- Chargement entête du document -----
$requete="
SELECT doc_number, 
third_civil_code, third_nom, third_prenom, 
third_adresse1, third_adresse2, 
third_codepostal, third_ville, pays_doc,
doc_tel, doc_email,
total_pos_ttc
FROM documents
WHERE doc_id='$doc_id'
";
// echo "xx $requete xx";
$result = $wpdb->get_results($requete);
// var_dump($result);
foreach ($result AS $row ) {
      $doc_number= $row->doc_number; 
      $client_civil_code= $row->third_civil_code; 
      $client_nom= $row->third_nom; 
      $client_prenom= $row->third_prenom; 
      $client_adresse1= $row->third_adresse1; 
      $client_adresse2= $row->third_adresse2; 
      $client_codepostal= $row->third_codepostal; 
      $client_ville= $row->third_ville; 
      $client_pays= $row->pays_doc; 
      $client_tel= $row->doc_tel; 
      $client_email= $row->doc_email; 
      $total_pos_ttc= $row->total_pos_ttc; 
}
print "<form autocomplete=off action=/activshop-paypal-order method=post>";
print "<INPUT type=HIDDEN name=session_id value=$session_id>";
print "<INPUT type=HIDDEN name=lang value=$lang>";
print "<INPUT type=HIDDEN name=doc_id value=$doc_id>";
echo "<input type=hidden name=total_pos_ttc value=$total_pos_ttc >";
echo "<input type=hidden name=delivery_mode_cocher_price value=$delivery_mode_cocher_price >";
//echo "xxx $delivery_mode_cocher_price xxx";

echo '<table style=width:840px>';
echo '<tr>';
echo "<td>Commande n°<br><b>".$doc_number."</b></td>";
// -------------------------- Saisie Nom --------------------
echo "<td>Civilité<br><b>".$client_civil_code."</b></td>";
// -------------------------- Saisie Nom --------------------
echo "<td>Nom<br><b>".$client_nom."</b></td>";
// -------------------- Saisie Prénom --------------------
echo "<td>Prénom<br><b>".$client_prenom.'</b></td>';
// ----------------------------------------------------------
echo '</tr><tr>';
echo "<td>Adresse de livraison<br><b>".$client_adresse1.'<br>'.$client_adresse2.'</b></td>';
// ----------------------------------------------------------
// ----------------------------------------------------------
echo "<td>Code Postal<br><b>".$client_codepostal.'</b></td>';

echo "<td>Ville : <br><b>".$client_ville.'</b></td>';

// ------- Pays -----
echo '<td>';
echo "Pays<br><b>".$client_pays.'</b></td>';
// ----------------------------------------------------------

echo '</tr>';
echo '<tr>';
// ----------------------------------------------------------
echo "<td>Téléphone<br><b>".$client_tel."</b></td>";
// ----------------------------------------------------------
echo "<td>Email<br><b>".$client_email."</b></td>";

echo '</tr>';
echo "</table>";



// tableau choix livraison
echo '<table style=width:840px>';
echo '<tr><th>Mode de livraison : </th><th>Transporteur : </th><th>Description : </th><th>Prix : </th></tr>';
// ------- Chargement des modes de livraison -----
$requete="
select delivery_mode_code, delivery_mode_descr, delivery_mode_price from delivery_modes dm";
$result = $wpdb->get_results($requete);
foreach ($result AS $row ) {
      $delivery_mode_code= $row->delivery_mode_code;
      $delivery_mode_descr= $row->delivery_mode_descr;
      $delivery_mode_price= $row->delivery_mode_price;

      echo "<tr style=background-color:$bgcolor>";
      $checked='';
      if ( $delivery_mode_cocher==$delivery_mode_code ) { $checked='checked'; }
     // echo "<input type=radio name=ChoixLivraison $checked onchange=submit()>";
      echo "<td><input type=radio name=ChoixLivraison $checked onClick=document.location.href='http://www.dupontsarl.store/activshop-paiement/?session_id=$session_id&doc_id=$doc_id&delivery_mode_cocher=$delivery_mode_code&delivery_mode_cocher_price=$delivery_mode_price'>";
      echo '</td>';
      echo '<td>'.$row->delivery_mode_code.'</td>';
      echo '<td>'.$row->delivery_mode_descr.'</td>';
      echo '<td style=text-align:right>'.$row->delivery_mode_price.'</td>';
      echo "</tr>";
}
echo "</table>";




echo '<table style=width:840px>';
echo '<tr><th>No ligne</th><th>Référence</th><th>Désignation</th><th>Px. TTC</th><th>Quantité</th><th>Total TTC</th></tr>';
// ------- Chargement des lignes du document -----
$requete="
SELECT pos_num,
pos_reference, pos_description, pos_puttc, pos_qte, total_ttc_pos
FROM doc_positions
WHERE doc_id='$doc_id'
";
// echo "xx $requete xx";
$result = $wpdb->get_results($requete);
// var_dump($result);
foreach ($result AS $row ) {
      $pos_num= $row->pos_num;
      $pos_reference= $row->pos_reference;
      $pos_description= $row->pos_description;
      $pos_puttc= $row->pos_puttc; 
      $pos_qte= $row->pos_qte;
      $total_ttc_pos= $row->total_ttc_pos; 

	echo "<tr style=background-color:$bgcolor>";
	echo '<td>'.$row->pos_num.'</td><td>'.$row->pos_reference.'</td>';
	echo '<td>'.$row->pos_description.'</td><td style=text-align:right>'.$row->pos_puttc.'</td>';
	echo '<td style=text-align:right>'.$row->pos_qte.'</td><td style=text-align:right>'.$row->total_ttc_pos.'</td>';
	echo '</tr>';
	$total_doc_qte=$total_doc_qte+$row->pos_qte;
}

echo '<tr style=font-weight:bold>';
echo '<td style=text-align:right colspan=4><b>Totaux</b></td>';
echo '<td style=text-align:right><b>'.$total_doc_qte.'</b></td>';
$total=$total_pos_ttc+$delivery_mode_cocher_price;
echo '<td style=text-align:right><b>'.$total.' €</b></td>';
//echo "xxx $total_pos_ttc xxx";
echo '</tr>';

echo '</table>';


print "<br>";
print "<p><input class=\"paypalButton\" type=\"image\" style=height:60px src=\"https://www.motoclic.pro/programs/images/frenchpaypal.png\" alt=\"Payer avec PayPal\" onclick=\"document.querySelector('#redirection').style.display='block';\"/>";
echo "                ";
print "<input class=\"paypalButton\" type=\"image\" style=height:60px src=\"https://www.motoclic.pro/programs/images/creditcards.gif\" alt=\"Payer avec PayPal\" onclick=\"document.querySelector('#redirection').style.display='block';\"/></p>";
print "<div id=redirection style=\"display:none;background-color:#EFE;border:1px solid #5F5;margin : 10px;padding:10px;color:#050;font-weight:bold;\" >Redirection vers PayPal en cours, veuillez patienter ...</div>";

echo "</form>"; // Fin DIV Panier

echo "</div>"; // Fin DIV Panier


echo "</div>"; // Fin DIV WIDGETS

echo "</div>"; // Fin DIV PARTIE HAUTE (DOCUMENT + WIDGETS)

?>

<?php get_footer(); // On affiche de pied de page du thème
?>

