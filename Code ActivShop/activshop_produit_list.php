<?php
/*
Template Name: activshop_produit_list
*/
// Boutique en lgne : Choix des produits
// Version 2021-05-09
get_header(); // On affiche l'en-tête du thème WordPress

include 'routines.php';

include 'activshop_style.css';
//echo "testtttt";
$session_id=$_REQUEST[session_id];  //echo "ss Session : $session_id ss<br>";
$doc_id=$_REQUEST[doc_id];
$reference=$_REQUEST[reference];
$description=$_REQUEST[description];
$manuf_id=$_REQUEST[manuf_id];
$prix_ttc=$_REQUEST[prix_ttc];
$good_family_id=$_REQUEST[good_family_id];
$order=$_REQUEST[order];
$order_sens=$_REQUEST[order_sens];


$recherche=$_REQUEST[recherche];
$prixMaxi=$_REQUEST[prixMaxi];
$prixMini=$_REQUEST[prixMini];
//echo "xxx $prixMini xxx";



?>

<style>

/*  Formulaire de recherche en haut de page */ 

fieldset {
    background-color:#CCCCCC;
    margin:auto;
    margin-top:auto;
    max-width:auto;
    padding-top:auto;
    border:1px solid #666;
    border-radius:10px;
    box-shadow:0 0 10px #666;
    height: auto;
color:black;
}

legend {
    padding: 1.2em 0.5em;
    border:1px solid green;
    color:green;
    background-color:white;
    font-size:90%;
    text-align:center;
    width: 20%;
}

button {
        background-color: #008CBA;
}

.parent {

  display: flex;

  flex-wrap: wrap;

  margin: 0 auto;

  width: 100%;
color:black;
	}

.child {

   flex: 1 0 21%; /* explanation below */

   overflow:hidden;

   margin:15px;

   max-height:auto;

   //background-color:#CCCCCC;

   /* margin:auto; */

   max-width:auto;

   padding-top:10px;

   border:1px solid #666;

   border-radius:10px;

   box-shadow:0 0 10px #666;

   cursor:pointer;
color: black;
}


</style>

<?php


//Début DIV PARTIE HAUTE (CARTOUCHE + WIDGETS)
echo "<div style=\"display : flex; flex-direction : row; justify-content : space-between;\">";

// Début DIV CARTOUCHE DOCUMENT
echo "<div style=\"display : inline-block\">";


echo '<center>';

echo "<fieldset style=margin-left:30%;width:700px><legend>Recherche</legend>";
echo "<form method=GET onsubmit='location.reload()'>";
//echo "<table style=width:800px class=trans >";

echo '<tr>';
// ---------------Sélection description --------------------
echo "<td>Recherche : <input style=width:55%; type=text name=recherche id=recherche value=\"$recherche\"></td>";
// ----------------------------------------------------------
echo "</tr>";

echo '<tr>';
echo "<td><br>";
// ------- Chargement liste des fabricants -----
echo "<td>Fabricant : <select style=width:55%; name=manuf_id id=manuf_id onchange=submit() ></td>";
$requete="
SELECT DISTINCT person_id, nom
FROM persons
WHERE role_manuf='o'
ORDER BY nom
";

$liste_manuf = $wpdb->get_results($requete);
foreach ($liste_manuf AS $manuf ) {
        if ( $manuf_id == $manuf->person_id ) { $manuf_nom= $manuf->nom; }
}

echo "<option value=".$manuf_id.">".$manuf_nom;
echo "<option value=TOUS>(Tous)";
foreach ($liste_manuf AS $manuf ) {
        echo "<option value=".$manuf->person_id.">".$manuf->nom;
}
echo '</select>';
// --- Fin Chargement liste des fabricants -----
echo '</td>';
echo '</tr>';

echo "<tr>";
// ---------------Sélection description --------------------
echo "<td><br> Prix mini : <input style=width:25%; type=number name=prixMini id=prixMini value=\"$prixMini\"></td>";
// ---------------Sélection description --------------------
echo "<td>  Prix maxi : <input style=width:25%; type=number name=prixMaxi id=prixMaxi value=\"$prixMaxi\"></td>";
// ----------------------------------------------------------
echo "</tr>";

//echo "</table>";
echo "<center><input  name=update type=submit id=update value=Rechercher></center>";
echo '</fieldset><br><br><br><br>';


echo "</div>";
// Fin DIV CARTOUCHE DOCUMENT

// Début DIV WIDGETS
echo "<div onclick=location.href='/activshop-panier-visu/?session_id=$session_id&doc_id=$doc_id' style=cursor:pointer;display:flex;flex-direction:column;justify-content:space-between; title=Voir le panier  >";

// --- Début DIV panier ---
if ( $doc_id <>'' ) { $onclick='onclick'; }
echo "<td class=panier onclick=location.href='/activshop-panier-visu/?session_id=$session_id&doc_id=$doc_id' title=Voir le panier >";
echo "<center><img style=width:40px src=/images/activshop_panier_logo.png>";
$requete="SELECT doc_number, COUNT(doc_pos_id) AS nb_lignes
FROM documents LEFT JOIN doc_positions ON documents.doc_id=doc_positions.doc_id
WHERE documents.doc_id='$doc_id'
";
$paniers = $wpdb->get_results($requete);
foreach($paniers as $panier) {
    echo '<div>'.$panier->doc_number.'</div>';
    echo '<div class=numberCircle>'.$panier->nb_lignes.'</div>';
}
echo '</div>'; 
// --- Fin DIV Panier ---

echo '</div>'; // Fin DIV WIDGETS

echo '</div>'; // Fin DIV PARTIE HAUTE (DOCUMENT + WIDGETS)


// ---- Compte du nombre total de produits ---------
$requete="SELECT COUNT(goods.good_id) AS nb_total_goods 
FROM goods 
WHERE
goods.vn_vo IS NULL
AND forfait IS NULL
";
$result = $wpdb->get_results($requete);
$nb_total_goods=$result[0]->nb_total_goods;
// ---- Fin Compte du nombre total de produits -----



$requete="
SELECT
goods.good_id,
goods.reference,
goods.description,
goods.prix_vente_ttc,
IFNULL(spos.stock_pos_qty,0) AS stock_pos_qty,
linked_files.linked_file_id,
linked_files.linked_file_name,
manuf.nom AS manuf_nom
FROM goods
LEFT JOIN linked_files ON goods.good_id=linked_files.linked_file_object_id
        AND linked_files.linked_file_object_type='GOOD'
LEFT JOIN persons AS manuf ON goods.manuf_id=manuf.person_id
LEFT JOIN stock_positions AS spos ON goods.good_id=spos.good_id AND goods.stock_location_id=spos.stock_location_id
WHERE goods.good_id IS NOT NULL
AND goods.vn_vo IS NULL
AND forfait IS NULL
AND goods.prix_vente_ttc > 0
";
//if ( $reference <>'' )	{ $requete.=" AND goods.reference LIKE '$reference%' "; }
if ( $prixMini <> '' && $prixMaxi == ''  ){ $requete.=" AND goods.prix_vente_ttc >= '$prixMini'"; }
if ( $prixMaxi <> '' && $prixMini == '' ){ $requete.=" AND goods.prix_vente_ttc <= '$prixMaxi'"; }
if ( $prixMini <> '' && $prixMaxi <> '' ){ $requete.=" AND goods.prix_vente_ttc >= '$prixMini' AND goods.prix_vente_ttc <= '$prixMaxi'"; }
if ( $manuf_nom <> '' ) { $requete.=" AND manuf.nom = '$manuf_nom'";  }
if ( $recherche <>'' ){ $requete.=" AND goods.description LIKE '%$recherche%' OR goods.reference LIKE '$recherche%' "; }

//if ( $order =='image' )			{ $requete.=" ORDER BY linked_file_name $order_sens "; }

$requete.=" LIMIT 50";
//echo "gg $requete gg<br>";

$produits = $wpdb->get_results($requete);
// print_r($produits);
?>

<?php
echo '<center>';

echo '<br><br><b>'.$wpdb->num_rows." produits / $nb_total_goods".'</b>';

//include "table_header.php";

echo "<div class=parent>";
foreach($produits as $produit) {

 if ( $bgcolor=='white' ) { $bgcolor='white'; } else { $bgcolor='white'; }
echo "<div class=child onclick=location.href='/activshop-produit-visu/?good_id=".$produit->good_id."&session_id=$session_id&doc_id=$doc_id' class=hover_dark style=background-color:$bgcolor;>";

   //echo "<td onclick=location.href='/activshop-produit-visu/?good_id=".$produit->good_id."&session_id=$session_id&doc_id=$doc_id' class=hover_dark style=background-color:$bgcolor>";
   echo '<b>Référence : '.$produit->reference.'</b><br>';
        if ( $produit->linked_file_name <> '' )
            { $image="/linked_files/".$produit->linked_file_id.'/'.$produit->linked_file_name; }
        else { $logo=`ls images/logo`; $image = "/images/logo/$logo"; }
   echo "<img style=width:200px;height:150px; src=$image><br><br>";
   echo ''.$produit->description.'<br>';
//   echo '<td>'.$produit->manuf_nom.'</td>';
if($produit->stock_pos_qty == 0)
        {
        echo "<p class=msg_error>Rupture de stock</p>";
        }
else
        {
         echo "<p class=msg_ok>En stock</p>";
        }
//   echo '<td>'.$produit->stock_pos_qty.'</td>';
   echo '<b>Prix : '.number_format($produit->prix_vente_ttc,  2, ',', ' ').'</b><br><br>';
   echo '</div>';
}
echo "</div>";
get_footer(); // On affiche de pied de page du thème

?>

