<?php
/*
Template Name: activshop_produit_visu
*/
// Version 2021-01-24

$session_id=$_REQUEST[session_id];
$doc_id=$_REQUEST[doc_id]; //echo "doc id : $doc_id dd ";
$good_id=$_REQUEST[good_id];

get_header(); // On affiche l'en-tête du thème WordPress

include 'activshop_style.css';

$quantite=1;

global $wpdb; // On se connecte à la base de données du site
$produits = $wpdb->get_results("
SELECT 
goods.reference,
goods.description,
goods.prix_vente,
goods.taux_tva_id,
taux_tva.taux,
goods.prix_vente_ttc,
IFNULL(spos.stock_pos_qty,0) AS stock_pos_qty,
manuf.nom AS manuf_nom,
linked_files.linked_file_id,
linked_files.linked_file_name
FROM goods
LEFT JOIN persons AS manuf ON goods.manuf_id=manuf.person_id
LEFT JOIN linked_files ON goods.good_id=linked_files.linked_file_object_id
	AND linked_files.linked_file_object_type='GOOD'
LEFT JOIN taux_tva ON goods.taux_tva_id=taux_tva.taux_tva_id
LEFT JOIN stock_positions AS spos ON goods.good_id=spos.good_id
    AND goods.stock_location_id=spos.stock_location_id
WHERE goods.good_id='$good_id'
");

// print_r($produits);
?>

<?php
echo '<center>';
print "<center><br><a href='/activshop-produit-list/?session_id=$session_id&doc_id=$doc_id' class=button>Retour</a></center>";
echo "<form action=/update-panier method=POST>";
echo "<input type=hidden name=session_id value=$session_id>";
echo "<input type=hidden name=doc_id value=$doc_id>";
echo "<input type=hidden name=good_id value=$good_id>";
echo "<input type=hidden name=transac value=insert>";
echo '<table class=choix style=width:80%>';
echo "<tr>";
echo "<th>Référence</th>";
echo "<th>Description</th>";
echo "<th>Fabricant</th>";
echo "<th>Image</th>";
echo "<th>Stock</th>";
echo "<th>Prix TTC</th>";
echo "<th>Quantité</th>";
echo "</tr>";
foreach($produits as $produit) {
   echo '<tr>';
   echo "<td>".$produit->reference.'</td>';
   echo "<td>".$produit->description.'</td>';
   echo "<td>".$produit->manuf_nom.'</td>';

   echo '<td style=text-align:center>';
   if ( $produit->linked_file_name <> '' )
            { $image="/linked_files/".$produit->linked_file_id.'/'.$produit->linked_file_name; }
   else { $logo=`ls images/logo`; $image = "/images/logo/$logo"; }
   echo "<img style=width:150px src=$image>";
   echo '</td>'; 
   echo '<td style=text-align:center>'; 
	if($produit->stock_pos_qty == 0)
	{
	echo "<p class=msg_error>Rupture<br>de<br>stock</p>";
	}
	else
	{
	 echo "<p class=msg_ok><br>En stock</p>";
	}
	echo '</td>';
	   // echo "<td style=text-align:right>".$produit->prix_vente."</td>";
	echo "<td style=text-align:right>".number_format($produit->prix_vente_ttc,  2, ',', ' ').'</td>';
	echo "<td style=text-align:right><input name=quantite id=quantite type=number size=5 min=1 max=50 value=$quantite></td>";
	echo '</tr>';

  /* else
	{
	echo '<p class=msg_ok>'.$produit->stock_pos_qty.'<br>en stock</p>'; 
	echo '</td>';
           // echo "<td style=text-align:right>".$produit->prix_vente."</td>";
        echo "<td style=text-align:right>eee".number_format($produit->prix_vente_ttc,  2, ',', ' ').'</td>';
        echo "<td style=text-align:right><input name=quantite id=quantite type=number size=5 min=1 max=50 value=$quantite></td>";
        echo '</tr>';

	}
*/
/*
   echo '</td>';

   // echo "<td style=text-align:right>".$produit->prix_vente."</td>";
   echo "<td style=text-align:right>".number_format($produit->prix_vente_ttc,  2, ',', ' ').'</td>';
   echo "<td style=text-align:right><input name=quantite id=quantite type=number size=5 min=1 max=50 value=$quantite></td>";
   echo '</tr>';
*/
}
echo '</table>';
//echo "www $produit->stock_pos_qty www";
$stockinfo= $produit->stock_pos_qty;
//echo "je test $stockinfo oui";
/*
if ( $produit->stock_pos_qty <= 0 )
        {
	echo "<center><input <a href=javascript:history.back() type=submit class=button onClick=alert('test'); value=\"Ajouter au panier\" >";
	echo '</form>';
	}
else
	{
	if ( $produit->stock_pos_qty <= 0 )
	{
	echo "<center><input type=submit onclick=alert('Produit enddddd') class=button value=\"Ajouter au panier\">";
	//<button type="button" onclick="alert('Hello World!')">Click Me!</button>
	}

	else
	{*/
	echo "<center><input type=submit class=button value=\"Ajouter au panier\" >";
	echo "<input type=hidden name=stockinfo value=$stockinfo>";
	echo "<input type=hidden name=reference value=$produit->reference>";
	echo "<input type=hidden name=description value=\"$produit->description\" >";
	echo "<input type=hidden name=prix_vente value=\"$produit->prix_vente\" >";
	echo "<input type=hidden name=taux_tva_id value=\"$produit->taux_tva_id\" >";
	echo "<input type=hidden name=prix_vente_ttc value=\"$produit->prix_vente_ttc\" >";
	echo "<input type=hidden name=taux_tva value=\"$produit->taux\" >";
	echo "<input type=hidden name=laquantite value=\"$quantite\" >";
	echo '</form>';
	//}
/*
echo "<input type=hidden name=reference value=$produit->reference>";
echo "<input type=hidden name=description value=\"$produit->description\" >";
echo "<input type=hidden name=prix_vente value=\"$produit->prix_vente\" >";
echo "<input type=hidden name=taux_tva_id value=\"$produit->taux_tva_id\" >";
echo "<input type=hidden name=prix_vente_ttc value=\"$produit->prix_vente_ttc\" >";
echo "<input type=hidden name=taux_tva value=\"$produit->taux\" >";
echo "<input type=hidden name=laquantite value=\"$quantite\" >";
echo '</form>';
*/
?>

<?php get_footer(); // On affiche de pied de page du thème
?>

