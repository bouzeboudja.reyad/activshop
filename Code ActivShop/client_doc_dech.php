<html>
<?php
// client_doc_dech.php
// Décharge (transformation) de documents clients
// Applis MotoClic
// Version 2021-02-13

include "routines.php";
$session_id=$_REQUEST['session_id'];
$debug=$_REQUEST['debug'];
$doc_id=$_REQUEST['doc_id'];
$type_doc_id=$_REQUEST['type_doc_son_id'];
$confirm=$_REQUEST['confirm'];
$lang=$_REQUEST['lang'];

include 'verif_session.php';

include 'db_connect.php';

// ----------- Chargement infos société -------------------------------
include "includes/load_soc_info.php";
// ---------------------------------------------------------------------

$titrepage='Décharger un document client';

print "<head>";
include_once $_SERVER['DOCUMENT_ROOT']."/programs/includes/style_load.php";
print "<title>$soc_nom_com - "; chtext ($titrepage); print "</title>";
print "</head>";

print "<body>";
print "<div id=conteneur>";

print "<div id=header>"; include "header.php"; echo '</div>';

print "<div id=frame>";
echo '<center>';

$now = date ("Y-m-j H:i:s");
$date_jour=date("Y-m-j");

// ------- Chargement table des types de documents ------------------
$requete="
SELECT type_doc_id, type_doc_ref, type_doc_descr 
FROM type_doc";
if ( $debug == '1' ) { print "-- $requete --</br>"; }
$d=0;
foreach( $dbh->query($requete) as $row ) {
	$type_doc_id_table[$d] =$row['type_doc_id'];
	$type_doc_ref_table[$d] =$row['type_doc_ref'];
	$type_doc_descr_table[$d] =$row['type_doc_descr'];
	if ( $type_doc_id_table[$d] == $type_doc_id ) {  $type_doc_descr = $type_doc_descr_table[$d] ; }
        $d++;
} # Fin de foreach
if ($nb_enr=='0' ) { print "<p class=msg_error>Aucun type de document trouvé.</p>"; exit; }
// --------------------------------------------------------------------

// ------ Recherche numéro et type du document source ------------------
$requete="
SELECT 
documents.doc_number, 
documents.type_doc_id,
type_doc.type_doc_descr
FROM documents 
LEFT JOIN type_doc ON documents.type_doc_id=type_doc.type_doc_id
WHERE doc_id= '$doc_id' ";
if ( $debug == '1' ) { print "-- $requete --</br>"; }
foreach( $dbh->query($requete) as $row ) {
	$num_source =$row['doc_number'];
	$type_doc_id_source =$row['type_doc_id'];
	$type_doc_descr_source =$row['type_doc_descr'];
} # Fin de foreach
if ($nb_enr=='0' ) { print "<p class=msg_error>Numéro de document source non trouve.</p>"; exit; }
$num_source_temp=$num_source."X"; 
print "Document source : $num_source <br>";
//  print "Numéro temporaire : $num_source_temp <br>";
// ---- Fin Recherche numéro et type du document source ----------------

// ------ Vérification s'il existe déjà un document enfant de ce type -------
$requete="
SELECT 
documents.doc_id, 
documents.doc_number, 
documents.type_doc_id,
type_doc_descr
FROM documents 
LEFT JOIN type_doc ON documents.type_doc_id=type_doc.type_doc_id
WHERE documents.type_doc_id= '$type_doc_id' 
AND doc_pere_id='$doc_id'
";
if ( $debug == 'e' ) { print "-- $requete --</br>"; }
$result=$dbh->query($requete);
$nb_enr=$result->rowCount();
if ($nb_enr<>'0' && $confirm <>'o' ) { 
print "<p class=msg_error>Attention il existe dejà $nb_enr $type_doc_descr pour le $type_doc_descr_source $num_source </p>"; }
if ( $type_doc_id=='9' ) { $prog='or_modif.php'; } else { $prog='client_doc_modif.php'; }
foreach ($result as $row) {
	$existing_doc_id =$row['doc_id'];
	$existing_doc_number =$row['doc_number'];
	$type_doc_id_source =$row['type_doc_id'];
	$type_doc_descr_dest =$row['type_doc_descr'];
	echo "<a class=lienbleu href=$prog?session_id=$session_id&lang=$lang&doc_id=$existing_doc_id>". $row['doc_number'].'</a><br>';
} # Fin de foreach
if ($nb_enr<>'0' && $confirm <>'o' ) { 
    echo "<br><a class=lienbleu href=".$_SERVER['REQUEST_URI']."&confirm=o>Créer quand-même un nouveau $type_doc_descr_dest</a>"; 
    print "<br><br><INPUT TYPE=button onclick=Javascript:history.go(-1) value=\"";chtext('Retour'); print "\">";
    exit; }
// ---Fin vérification s'il existe déjà un document enfant de ce type -------

// ------- Calcul d'un nouveau numéro de document --------
$requete="SELECT type_doc_ref, prefix, doc_number, suffix
FROM type_doc
WHERE type_doc_id='$type_doc_id'";
print "<p>Récupération d'un nouveau numéro de document...</p>";
if ($debug == "1") {print "-- $requete --<br>";}
foreach( $dbh->query($requete) as $row ) {
	$type_doc_ref=$row['type_doc_ref'];
        $prefix=$row['prefix'];
        $lastnumdoc =$row['doc_number'];
        $suffix =$row['suffix'];
} # Fin de foreach
// print "<br>--- DERNIER NUMERO de $type_doc_ref :  $lastnumdoc ---";
$nouveaunum=($lastnumdoc+1);
$nouveaudocnum=sprintf("%05d",$nouveaunum);
# print "<br>--- NOUVEAU n° $nouveaunum ---";
$new_doc_number="$prefix$nouveaudocnum$suffix";
// print "<br>NOUVEAU DOCUMENT : $new_doc_number<br>";
// -----------------------------------------------------------

// ------ création du document --------------------------------
$requete="
INSERT INTO documents (
doc_number, type_doc_id, doc_status_code, third_id, date_doc, 
third_civil_code, third_nom, third_prenom,
third_adresse1, third_adresse2, third_codepostal, third_ville, pays_doc,
doc_tel, doc_mobile, doc_email,
doc_contact_civil ,doc_contact_nom, doc_contact_prenom, 
vehicule_id, 
vehicule_immatriculation, 
vehicule_serial_number, 
vehicule_modele, 
vehicule_manuf_id, 
vehicule_manuf_nom, 
vehicule_cnit, 
vehicule_kilometrage, 
vehicule_nbHr,
vehicule_annee_modele, 
vehicule_etat, 
vehicule_carburant_niveau,
good_color_id,
date_debut_asked, date_fin_asked,
taux_tva,total_ht, total_tva, taux_remise, montant_remise, total_pos_ttc, 
net_a_payer, mode_regl, cond_regl, cond_regl_id, doc_pere_id, doc_pere_number, total_qty, date_delivery, 
date_payment, ref_doc, accompte, affair_id,
date_cre, pseudo ) 
SELECT 
'$new_doc_number','$type_doc_id', '1', third_id, '$date_jour',
third_civil_code, third_nom, third_prenom,
third_adresse1, third_adresse2, third_codepostal, third_ville, pays_doc,
doc_tel, doc_mobile, doc_email,
doc_contact_civil ,doc_contact_nom, doc_contact_prenom, 
vehicule_id, 
vehicule_immatriculation, 
vehicule_serial_number, 
vehicule_modele, 
vehicule_manuf_id, 
vehicule_manuf_nom, 
vehicule_cnit, 
vehicule_kilometrage, 
vehicule_nbHr,
vehicule_annee_modele, 
vehicule_etat,
vehicule_carburant_niveau,
good_color_id,
'$date_jour 09:00:00', '$date_jour 17:00:00', 
taux_tva,total_ht, total_tva, taux_remise, montant_remise, total_pos_ttc, 
net_a_payer , mode_regl, cond_regl, cond_regl_id, doc_id, doc_number, total_qty, '$date_jour', 
'$date_jour', ref_doc, accompte, affair_id,
'$now', '$pseudo'
FROM documents 
WHERE documents.doc_id ='$doc_id'";

print "<p>Copie du document...</p>";
if ($debug == "1") {print "$requete<br>";}
$dbh->query($requete);

// -------------------------------------------------------

// ------ Récupération de l'id du nouveau document -------------
$new_doc_id =  $dbh->lastInsertId();
// -------------------------------------------------------------

// ---------------- Copie des positions -------------------------
$requete="
INSERT INTO 
doc_positions 
(
pos_reference, pos_type, good_id, pos_description, pos_pere_id, vn_vo,
vehicule_serial_number, vehicule_modele, vehicule_immatriculation, vehicule_kilometrage, vehicule_nbHr, manuf_nom,
pos_qte, pos_puht, pos_puttc, taux_tva, pos_total_tva, doc_id, remise_pourcent, total_ht_pos, total_ttc_pos, 
vehicule_prix_achat_ttc, serial_number, vehicule_version, date_mise_circulation, piece_identite_descr, date_entree,
date_delivery, taux_tva_id, pos_num, financial_account_number, batch_number, date_cre
) 
SELECT 
pos_reference, pos_type, good_id, pos_description, doc_pos_id, vn_vo,
vehicule_serial_number, vehicule_modele, vehicule_immatriculation, vehicule_kilometrage, vehicule_nbHr, manuf_nom,
pos_qte,pos_puht, pos_puttc, taux_tva, pos_total_tva, $new_doc_id, remise_pourcent, total_ht_pos, total_ttc_pos,
vehicule_prix_achat_ttc, serial_number, vehicule_version, date_mise_circulation, piece_identite_descr, date_entree,
date_delivery, taux_tva_id, pos_num, financial_account_number, batch_number, '$now'
FROM 
doc_positions 
WHERE doc_id='$doc_id'";

print "<p>Copie des lignes...</p>";
if ($debug == "1") {print "$requete<br>";}
$dbh->query($requete);
// ----------------------------------------------------------------

// ----------------Sélection de l'id des lignes lignes de textes du document source ----------

$requete = "
SELECT doc_pos_id
FROM doc_positions
WHERE doc_id='$doc_id'
AND pos_type='T'";

$result=$dbh->query($requete);
$nb_enr=$result->rowCount();
$i=0;
foreach ($result as $row) {
	$old_texte_doc_pos_id[$i]=$row['doc_pos_id'];
	$i++;
}

// ------------------------------------------------------------------------


// ----------------Sélection de l'id des nouvelles lignes de textes crées ----------

$requete = "
SELECT doc_pos_id
FROM doc_positions
WHERE doc_id='$new_doc_id'
AND pos_type='T'";

$result=$dbh->query($requete);
$nb_enr=$result->rowCount();
$i=0;
foreach ($result as $row) {
	$new_texte_doc_pos_id[$i]=$row['doc_pos_id'];
	$i++;
}

// ------------------------------------------------------------------------

// ---------------- Copie des textes -------------------------
print "<p>Copie des textes...</p>";
$i=0;
foreach ($old_texte_doc_pos_id as $old_texte_doc_pos_id[$i]) {
	
	$requete="
	INSERT INTO 
	doc_textes 
	(
	doc_texte_ref, doc_texte_descr,
	doc_pos_id,
	date_cre,
	user_cre_id
	) 
	SELECT 
	doc_textes.doc_texte_ref, doc_textes.doc_texte_descr,
	'$new_texte_doc_pos_id[$i]',
	'$now',
	'$user_id'
	FROM 
	doc_textes
	WHERE doc_textes.doc_pos_id='$old_texte_doc_pos_id[$i]'";

	if ($debug == "1") {print "$requete<br>";}
	$dbh->query($requete);
	$i++;
}

// ----------------------------------------------------------------


// ---------------- Copie du récap TVA -------------------------
$requete="
INSERT INTO
recap_tva (
doc_id,
total_taux_tva,
taux_tva_id,
date_cre,  
date_modif,
user_cre,   
user_modif,
total_ht,  
taux_tva)
SELECT 
$new_doc_id,
total_taux_tva,
taux_tva_id,
date_cre,
date_modif,
user_cre,
user_modif,
total_ht,
taux_tva
FROM recap_tva
WHERE doc_id='$doc_id'";
print "Copie du recap TVA...<br>";
if ($debug == "1") {print "$requete<br>";}
$dbh->query($requete);
// ----------------------------------------------------------------

/*
// ---------------- Copie des règlements -----------------
$requete="
INSERT INTO
doc_payments
(
acompte, date_payment, payment_amount, mode_regl_id, doc_id, date_cre
)
SELECT
acompte, date_payment, payment_amount, mode_regl_id, $new_doc_id, '$now'
FROM
doc_payments
WHERE doc_id='$doc_id'";

print "<p>Copie des règlements...</p>";
if ($debug == "1") {print "$requete<br>";}
$dbh->query($requete);
// --------------------------------------------------------
*/

// ----------- MAJ Compteur Document ----------------------
$requete="
UPDATE type_doc 
SET doc_number='$nouveaunum' 
WHERE type_doc_id='$type_doc_id';";
print "<p>Mise à jour du compteur de documents...</p>";
if ($debug == "1") {print "$requete<br>";}
$dbh->query($requete);
// --------------------------------------------------------

// ----------- Solder le document source ------------------

if ( $type_doc_id_source=='9' ) { $doc_status_code='11'; } // Si le document source est un OR, on le passe en statut "Terminé"
else 				{ $doc_status_code='4';  } // Sinon si le document source est un devis on le passe au statut "Soldé"

$requete="
UPDATE documents
SET doc_status_code='$doc_status_code'
WHERE doc_id='$doc_id';";
print "<p>Solde du document source...</p>";
if ($debug == "1") {print "$requete<br>";}
$dbh->query($requete);
// --------------------------------------------------------

print "<p class=msg_ok>"; chtext ('Votre document a bien été déchargé'); print ". "; chtext("Nouveau document"); print":</p>";

if ( $type_doc_id =='9' ) { // O.R
	print "<p><A HREF=/programs/or_modif.php?lang=$lang&session_id=$session_id&doc_id=$new_doc_id>$new_doc_number</A></p>";
} else {
	print "<p><A HREF=/programs/client_doc_modif.php?lang=$lang&session_id=$session_id&doc_id=$new_doc_id>$new_doc_number</A></p>";
}

print "<INPUT TYPE=\"button\" onClick=\"parent.location='client_doc_choix.php?lang=$lang&session_id=$session_id'\" VALUE=\""; chtext('Retour'); print "\">";

print "</div>";

print "</div>";
?>
</BODY>
</HTML>
